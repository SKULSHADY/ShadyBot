from datetime import datetime, timedelta
from requests import get
from telegram import Bot, InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import run_async
from tg_bot.modules.disable import DisableAbleCommandHandler
from tg_bot import dispatcher
import json
import os


base_url = "https://raw.githubusercontent.com/Havoc-OS/OTA/eleven/vanilla/{}.json"
devices_url = "https://raw.githubusercontent.com/Havoc-OS/official_devices/eleven/devices"
json_path = "/home/havoc/bot/ShadyBot/devices.json"


def fetch_devices():
    response = get(devices_url)
    content = response.content.decode("utf-8")
    device_names = content.strip().split("\n")
    devices = {}

    for device_name in device_names:
        response = get(base_url.format(device_name))

        if response.status_code == 404:
            print("JSON not found for ", device_name)
            continue

        content = response.content.decode("utf-8")

        try:
            device_json = json.loads(content)
            devices[device_name] = device_json
        except:
            print("Invaild JSON: ", device_name)

    print("Fetched", len(devices), "devices")

    with open(json_path, "w") as f:
        json.dump(devices, f)

    return devices


def is_file_older_than(file, delta):
    cutoff = datetime.utcnow() - delta
    mtime = datetime.utcfromtimestamp(os.path.getmtime(file))

    if mtime < cutoff:
        return True

    return False


def update_devices():
    if os.path.exists(json_path):
        if is_file_older_than(json_path, timedelta(seconds=600)):
            devices = fetch_devices()
        else:
            with open(json_path, "r") as f:
                devices = json.load(f)
    else:
        devices = fetch_devices()

    return devices


@run_async
def device(bot: Bot, update: Update):
    devices = update_devices()
    message = update.effective_message
    text = message.text[len("/device ") :].strip()

    try:
        deviceinfo = devices[text]
    except:
        if text != "":
            reply_error_text = (
                "Sorry, but %s isn't on our official devices list." % text
            )
        else:
            reply_error_text = "Please, specify your device by using /device `codename` (example `/device guacamole`)"

        message.reply_text(reply_error_text.replace("_", "\_"), parse_mode="Markdown")
        raise

    download_url = f"https://havoc-os.com/device#{text}"
    telegram_url = f'{deviceinfo["group"]}'
    reply_text = f'*Havoc-OS for {deviceinfo["name"]} ({text})*\n*Maintainer:* {deviceinfo["maintainer"]}\n*Latest build:* `{deviceinfo["filename"]}`'

    device_links = [
        [
            InlineKeyboardButton("Download", url=download_url),
            InlineKeyboardButton("Telegram Group", url=telegram_url),
        ]
    ]

    message.reply_text(
        reply_text.replace("_", "\_"),
        parse_mode="Markdown",
        reply_markup=InlineKeyboardMarkup(device_links),
    )


@run_async
def devicelist(bot: Bot, update: Update):
    devices = update_devices()
    message = update.effective_message
    info = "<b>List of currently supported devices:</b>"
    footer = "\n\n <b>Use /device codename to show more info.</b>"

    for device in devices:
        info += "\n- " + device

    message.reply_text(info + footer, parse_mode="HTML")


__help__ = """
 - /device:{word} Show info about a officially supported device. example: /device guacamole.\n
 - /devicelist: See the list of supported devices.
"""

__mod_name__ = "Device List"

devlist_handle = DisableAbleCommandHandler("device", device)
devlistinfo_handle = DisableAbleCommandHandler("devicelist", devicelist)

dispatcher.add_handler(devlist_handle)
dispatcher.add_handler(devlistinfo_handle)
